import logo from './logo-small.svg';
import logoBig from './logo-big.svg'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button, Container, Nav, Navbar, NavDropdown} from "react-bootstrap";

function App() {
  return (
    <div className="App">
      < Navigation />
        <div className = "bg">
            <img
                src={logoBig} alt="cur" class="center"
                height={350}
                width={700}
            />
            <span className = "middle-text"> Enjoy your journey! </span>
            <Button href="#" variant="outline-dark">Start</Button>
        </div>
        <Footer/>
    </div>
  );
}

function Navigation() {
  return (
      <Navbar className="color-nav text-footer" variant="light" expand="lg">
          <Navbar.Brand className = "top-logo" href="#home">
              <img
                src = {logo}
                width="270"
                height="40"
                alt="Metadrive logo"
              />
          </Navbar.Brand>
      </Navbar>
  )
}

function Footer() {
    return (
    <Navbar className="color-nav" variant="dark">
        <Nav className="container-fluid justify-content-xl-end">
            <Nav.Item className = "middle-elem">
                <Nav.Link>Legal Imprint</Nav.Link>
            </Nav.Item>
            <Nav.Item className = "middle-elem">
                <Nav.Link>Privacy Policy</Nav.Link>
            </Nav.Item>
            <Nav.Item className="ml-auto last-elem">
                <Nav.Link>Copyrights @ MetaDrive 2021</Nav.Link>
            </Nav.Item>
        </Nav>
    </Navbar>
    )
}

export default App;
